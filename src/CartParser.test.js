import CartParser from './CartParser';

let parser, parse, validate, parseLine, calcTotal


beforeEach(() => {
	parser = new CartParser();
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal;
});

describe('CartParser - unit tests', () => {
	describe('validation', () => {
	it('should not return error when validation passed', () => {
		const data = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;
		const errors = validate(data);
		expect(errors).toEqual([]);
	});

	it('should throw error when quantity is negative', () => {
		parser.readFile = jest.fn(() => `Product name,Price,Quantity
											Tvoluptatem,10.32,-1`);
		expect(parse).toThrow('Validation failed!');								
	})

	it('should return error with ErrorType.HEADER when header validation failed', () => {
		const data = `Product name!!!,Price,Quantity
		Mollis consequat,9.00,2`;
		const errors = validate(data);
		expect(errors[0].type).toBe(parser.ErrorType.HEADER);
	});

	it('should return error with ErrorType.ROW when row validation failed', () => {
		const data = `Product name,Price,Quantity
		Mollis consequat!!!9.00,2`;
		const errors = validate(data);
		expect(errors[0].type).toBe(parser.ErrorType.ROW);
	});

	it('should return error with ErrorType.CELL when cell validation failed', () => {
		const data = `Product name,Price,Quantity
		Mollis consequat, 9.00!,2`;
		const errors = validate(data);
		expect(errors[0].type).toBe(parser.ErrorType.CELL);
	});
});


	describe('parsing data', () => {
	  it('should return parsed array of items', () => {
		const data = `Product name,Price,Quantity
		Scelerisque lacinia,20,2
		Mollis consequat,10,6`;
		const readFileMock = jest.fn(path => data);
		parser.readFile = readFileMock;
		const testPath = './test.csv';
		const result = parser.parse(testPath);

		expect(result.items[0]).toHaveProperty('name', 'Scelerisque lacinia');
		expect(result.items[0]).toHaveProperty('price', 20);
		expect(result.items[0]).toHaveProperty('quantity', 2);
		expect(result.items[1]).toHaveProperty('name', 'Mollis consequat');
		expect(result.items[1]).toHaveProperty('price', 10);
		expect(result.items[1]).toHaveProperty('quantity', 6);
	});

	it('should return total price when data is valid', () => {
		const data = `Product name,Price,Quantity
		Scelerisque lacinia,20,2
		Mollis consequat,10,6`;
		const readFileMock = jest.fn(path => data);
		parser.readFile = readFileMock;
		const testPath = './test.csv';
		const result = parser.parse(testPath);

		expect(result.total).toEqual(100);
	});

	it("should return an item object with props: id, name, price, quantity", () => {
		const csvLine = `Product,10,5`;
		const item = parseLine(csvLine);
		expect(item.id).toBeDefined();
		expect(item.name).toEqual('Product');
		expect(item.price).toEqual(10);
		expect(item.quantity).toEqual(5);
	  });
	});

	describe("calcTotal", () => {
        it("should return correct total price", () => {
            const items = [
                {
					"id": '1',
                    "name": 'product1',
                    "price": 10,
                    "quantity": 5
                },
                {
					"id": '2',
                    "name": 'product2',
                    "price": 25,
                    "quantity": 2
                }
            ];

			expect(calcTotal(items)).toEqual(100);
        })

    });

});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should return JSON object when contents are valid', () => {
		const result = parse('../samples/cart.csv');
		expect(result.items).toBeDefined();
		expect(result.items.length).toEqual(5);
		expect(result.total).toBeCloseTo(348.32);
	})

});